FROM alpine AS gitclone

RUN apk update && apk add git && git clone -b MOODLE_37_STABLE git://git.moodle.org/moodle.git 

FROM php:7.3-apache 

RUN apt-get update && apt-get install -y libicu-dev zlib1g-dev libzip-dev && docker-php-ext-install intl zip && apt-get clean

COPY --from=gitclone /moodle .
